import UIKit

// starting 9:56:13
// wipe everything at 10:40 after trying to do something stupid with generics, starting again.
// Timestamps are assumed unique

// MARK: - Convenience
extension Set {
    func setmap<U>(transform: (Element) -> U) -> Set<U> {
        return Set<U>(self.lazy.map(transform))
    }
}

/// Last Write Wins Set Element
///
/// Contains a value and a timestap corresponding the it's creation
struct LWWSetElement<T: Hashable>: Equatable, Hashable {
    let value: T
    let timestamp: Date
    
    static func == (lhs: LWWSetElement<T>, rhs: LWWSetElement<T>) -> Bool {
        return lhs.value == rhs.value
    }
    
    /// Additional timestamp check
    static func === (lhs: LWWSetElement<T>, rhs: LWWSetElement<T>) -> Bool {
        return lhs.value == rhs.value
    }
    
    func isPosterior(to setElement: LWWSetElement) -> Bool {
        return timestamp > setElement.timestamp
    }
}

/// Last Write Wins Set implementation
struct LWWSet<T: Hashable> {
    public typealias SetElement = LWWSetElement<T>
    
    private var additions = Set<SetElement>()
    private var removals = Set<SetElement>()
    
    public var description: String {
        return resultingSet.description
    }
    
    public var debugDescription: String {
        let descriptionAdditions = additions.sorted { $0.timestamp < $1.timestamp }
            .map { "Value: \($0.value), Timestamp: \($0.timestamp.timeIntervalSince1970)" }
        let descriptionRemovals = removals.sorted { $0.timestamp < $1.timestamp }
            .map { "Value: \($0.value), Timestamp: \($0.timestamp.timeIntervalSince1970)" }
        
        return " - Additions:\n\(descriptionAdditions)\n - Removals\n\(descriptionRemovals)"
    }
    
    // Note: Nicer API but may not be right to have this as a var as it involve some work..
    /// Return the resulting Set, following :
    /// > "is in the add set, and either not in the remove set, or in the remove set but with
    /// > an earlier timestamp than the latest timestamp in the add set"
    public var resultingSet: Set<T> {
        return additions.filter { !operationWins($0, in: removals) }
            .setmap(transform: { $0.value })
    }
    
    // MARK: - Public methods
    
    /// Registers an addition operation
    ///
    /// - Parameter value: The value to add
    public mutating func add(_ value: T) {
        let setElement = SetElement(value: value, timestamp: Date())
        additions.insert(setElement)
    }
    
    /// Registers a removal operation
    ///
    /// - Parameter value: The value to remove
    public mutating func remove(_ value: T) {
        let setElement = SetElement(value: value, timestamp: Date())
        removals.insert(setElement)
    }
    
    // 12:29 realize that `removals.merge(lwwSet.removals); additions.merge(lwwSet.additions)` won't make it
    // as `==` doesn't care of timestamp and union uses it
    /// Merge another existing set
    public mutating func merge(_ lwwSet: LWWSet<T>) {
        let removalsToAdd = lwwSet.removals.filter { !operationWins($0, in: removals) }
        
        removals.union(removalsToAdd)
        
        let additionsToAdd = lwwSet.additions.filter { operationWins($0, in: additions) }
        
        additions.union(additionsToAdd)
        
        //12:56 - 3 hours done
        // I realise now that my implementation won't work because my LWWSet is base on default Set and I compare using `==` on values,
        // so I will never be able to store two values with != timestamps....
    }

    // MARK: - Private methods
    
    // Check if given operation wins, regarding another set of operations
    private func operationWins(_ operation: SetElement, in set: Set<SetElement>) -> Bool {
        return set.contains { $0.value == operation.value && $0.isPosterior(to: operation) }
    }
}

// 11:48: creative tests.. I'm in a playground so let's try it this way :)
// would have done it with proper XCTAssertXX instead of only assert for clearer log though
struct LastWriteWinsSetTest {
    
    enum Tokens: String, CaseIterable {
        case one = "Hi"
        case two = "I"
        case three = "Am"
        case four = "Alex"
    }
    
    static func tokensFullLWWSet() -> LWWSet<Tokens> {
        var lwwSet = LWWSet<Tokens>()
        
        Tokens.allCases.forEach { lwwSet.add($0) }
        return lwwSet
    }
    
    static func testInitialization() {
        let set = LastWriteWinsSetTest.tokensFullLWWSet()
        
        Tokens.allCases.forEach { assert(set.resultingSet.contains($0)) }
    }
    
    static func testAddExistingToken() {
        var set = LastWriteWinsSetTest.tokensFullLWWSet()
        
        set.add(.four)
        Tokens.allCases.forEach { assert(set.resultingSet.contains($0)) }
        assert(set.resultingSet.count == Tokens.allCases.count)
    }
    
    static func testRemoval() {
        var set = LastWriteWinsSetTest.tokensFullLWWSet()
        
        set.remove(.four)
        assert(!set.resultingSet.contains(.four))
        assert(set.resultingSet.count == (Tokens.allCases.count - 1))
    }
    
    static func testMerge() {
        var setA = LastWriteWinsSetTest.tokensFullLWWSet()
        var setB = LastWriteWinsSetTest.tokensFullLWWSet()
        
        setA.remove(.one)
        setB.remove(.one)
        setB.add(.one)
        
        print("setA debugDesc :\n")
        print(setA.debugDescription)
        print("\nsetB debugDescription :\n")
        print(setB.debugDescription)
        
        setA.merge(setB)
        
        print("\n -- setA post merge debugDescription :\n")
        print("\(setA.debugDescription)\n")

    }
    
}

LastWriteWinsSetTest.testInitialization()
LastWriteWinsSetTest.testAddExistingToken()
LastWriteWinsSetTest.testRemoval()
LastWriteWinsSetTest.testMerge()
